# Technical test

Voici le rendu du test technique backend

## Exercice 1

Le code de ce repository correspond à l'exercice 1. Ceci est ma première expérience avec Elixir et je serais ravie d'avoir vos retours concernant les bonnes pratiques que j'ai manquées. Bonne relecture 😘!

Pour les tests
```sh
mix test
```

Pour démarrer l'application :
```sh
iex -S mix
Erlang/OTP 24 [erts-12.2] [source] [64-bit] [smp:12:12] [ds:12:12:10] [async-threads:1] [jit]

Interactive Elixir (1.13.0) - press Ctrl+C to exit (type h() ENTER for help)

iex(1)> Welcome.run("technical-test-professions.csv", "technical-test-jobs.csv", "continents.json")
```

## Question 2
![schema](schema.svg "schema archi")

- `Jobs bucket` : Il s'agit de l'endroit où sont stockés les fichiers CSV d'emplois. Je propose ici de les stocker dans un bucket car, d'après l'énoncé, on peut recevoir une quantité assez importante de données chaque seconde. Un bucket semble être une bonne idée au cas où la mécanique serait grippée.
- `Archive bucket`⁣ : Un bucket miroir au `Jobs bucket` qui permet de séparer les fichiers CSV qui ont été lus de ceux à lire. Permet d'éviter au `Reader` de ne pas relire plusieurs fois les mêmes fichiers et permet d'avoir une archive au cas où il faudrait tout rejouer.
- `Reader`: Une tâche persistante qui accède au `jobs bucket` et qui lit le contenu des fichiers CSV. Lors de la lecture, cette tâche redécoupe l'information (chunks) afin d'en extraire des subsets, puis de les envoyer à des tâches de calcul de statistiques* et enfin, d'enregistrer le fichier lu dans le `Archive bucket`. La granularité ici est importante. De la taille des chunks va dépendre le temps d'exécution des différentes tâches et de l' `Aggregation task`. 
Pour simplifier le schéma, cette tâche est unique, ce qui peut fonctionner. En revanche, si la réception des fichiers est trop longue, on peut imaginer séparer cette tâche en deux afin d'avoir une qui "regarde" les changements sur le `Jobs bucket` et qui lancerait des tâches de lecture en conséquence.
- `Compute task`⁣ : ce sont les tâches de calcul de statistiques comme le ferait l'application de l'exercice 1. L'objectif ici, est de lancer le calcul des statistiques en parallèle sur les différents subsets afin de réduire au maximum le temps de calcul. Le résultat sera stocké dans le `Result backend`
- `Result backend`⁣ : C'est la couche de persistance des différentes statistiques calculées par les `Compute task`. Il peut s'agir d'une base de données, d'un bucket ou d'une base clé-valeur (comme Redis**)
- `Aggregation task`⁣ : Tâche persistante qui récupère les statistiques stockées dans le `Result backend` ainsi que les dernières statistiques stockées dans la base de données afin de les agréger et de stocker le résultat en base de données. Attention ici, il est important que la tâche d'agrégation soit unique, car plusieurs tâches pourraient se marcher sur les pieds, que ce soit en termes de calcul (prendre les mêmes statistiques) qu'en termes de stockage (ne pas se rendre compte qu'une autre tâche a mis à jour les informations stockées en base).
-  `DB` : stocke le résultat final afin de le rendre accessible instantanément. La fraicheur de cette donnée dépendra de l'optimisation de la chaîne de traitement.

**nb** : La solution proposée ainsi que les technologies mentionnées sont celles que j'utilise au quotidien. J'ai cru comprendre qu'Elixir (enfin Erlang) simplifiait énormément la mise en place de ce genre d'architecture.

\*: La tâche peut être créée directement par le `Reader` ou on peut passer par un système de `distributed task queue` avec une gestion file d'attente et de workers comme [celery](https://docs.celeryproject.org/en/stable/).

\*\*: Attention à la persistance dans ce cas.
## Question 3
Je propose une API renvoyant les données en JSON contenant les endpoints suivants:

- `/jobs`
    - format: `application/json`
    - code: `200`
    - result: Collection of `Job`
    - queryparams:
	    - category_id (ex: `1`)
	    - name (ex: `Compliance%20Manager`)
	    - contract_type (ex: `INTERNSHIP`)
	    - latitude (ex: `48.8867578`)
	    - longitude (ex: `2.3253786`)
	    - bounding_box (ex: `-0.489%2C51.28%2C0.236%2C51.686`)

- `/professions`
	- format: `application/json`
	- code: `200`
	- result: Collection of `Profession`
	- queryparams:
		- name (ex: `Developer`)
		- category_name (ex: `Tech`)

- `/professions/<id>`
	- format: `application/json`
	- code: `200`
	- result: `Profession`
	
- `/jobs/stats`
	- format: `application/json`
	- code: `200`
	- result: Collection of `ContinentStats`
	- queryparams:
		- continent (ex: `europe`)
		- category (ex: `Tech`)

### Schemas
#### Job
```
{
    "profession_id": int,
    "contract_type": string,
    "name": string,
    "office_latitude": string,
    "office_longitude": string
}
```
#### Profession
```
{
    "id": int,
    "name": string,
    "category_name": string
}
```
#### Stats
```
{
    "category_name": string,
    "count": int
}
```

#### ContinentStats
```
{
    "continent": string,
    "categories": [
	    Stats
    ],
    "name": string,
    "category_name": string,
}
```
