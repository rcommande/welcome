defmodule Welcome.MixProject do
  use Mix.Project

  def project do
    [
      app: :welcome,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:geo, "~> 3.4"},
      {:jason, "~> 1.2"},
      {:nimble_csv, "~> 1.2"},
      {:table_rex, "~> 3.1.1"},
      {:topo, "~> 0.4.0"}
    ]
  end
end
