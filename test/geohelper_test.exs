defmodule GeoHelperTest do
  use ExUnit.Case, async: true
  doctest GeoHelper

  setup_all do
    {:ok, geoms: GeoHelper.read_geometries("continents.json")}
  end

  test "Test get_continent with coordinates from Europe", context do
    lat = 48.8050106
    lng = 2.4406
    assert GeoHelper.get_continent(context[:geoms], lat, lng) == {:ok, :europe}
  end

  test "Test get_continent with coordinates from Pekin", context do
    lat = 39.9390628
    lng = 116.257378
    assert GeoHelper.get_continent(context[:geoms], lat, lng) == {:ok, :asia}
  end

  test "Test get_continent with coordinates from Africa", context do
    lat = -31.0305029
    lng = 23.8213658
    assert GeoHelper.get_continent(context[:geoms], lat, lng) == {:ok, :africa}
  end

  test "Test get_continent with coordinates from Australia", context do
    lat = -24.9971012
    lng = 115.2624813
    assert GeoHelper.get_continent(context[:geoms], lat, lng) == {:ok, :oceania}
  end

  test "Test get_continent with coordinates from America", context do
    lat = 37.2762522
    lng = -104.6480541
    assert GeoHelper.get_continent(context[:geoms], lat, lng) == {:ok, :north_america}
  end

  test "Test get_continent with coordinates from Brazil", context do
    lat = -14.216437
    lng = -60.3358498
    assert GeoHelper.get_continent(context[:geoms], lat, lng) == {:ok, :south_america}
  end

  test "Test get_continent with coordinates from Ocean", context do
    lat = 35.6583407
    lng = -51.6714889
    assert GeoHelper.get_continent(context[:geoms], lat, lng) == {:error, :lat_lng_error}
  end
end
