defmodule StatsTest do
  use ExUnit.Case, async: true
  doctest GeoHelper

  test "Stats generation" do
    jobs = [
      %{category_name: "Tech", continent: :europe},
      %{category_name: "Conseil", continent: :north_america},
      %{category_name: "Conseil", continent: :north_america},
      %{category_name: "Tech", continent: :unlocalized},
      %{category_name: "unknown", continent: :unlocalized}
    ]

    result = Stats.compute(jobs)

    assert result == %{
             :europe => %{"Tech" => 1, "Total" => 1},
             :asia => %{"Total" => 0},
             :north_america => %{"Conseil" => 2, "Total" => 2},
             :south_america => %{"Total" => 0},
             :africa => %{"Total" => 0},
             :oceania => %{"Total" => 0},
             :unlocalized => %{"Tech" => 1, "unknown" => 1, "Total" => 2},
             :total => %{"Tech" => 2, "Conseil" => 2, "unknown" => 1, "Total" => 5}
           }
  end
end
