defmodule ProfessionReaderTest do
  use ExUnit.Case, async: true
  doctest GeoHelper

  setup_all do
    project_dir = File.cwd!()
    test_csv_path = Path.join([project_dir, "test", "data", "professions.csv"])
    {:ok, test_csv_path: test_csv_path}
  end

  test "Read profession csv file", context do
    test_file = File.stream!(context[:test_csv_path])
    result = ProfessionReader.get_profession_mapping(test_file) |> Enum.to_list()
    assert length(result) === 1
    assert List.first(result) === {1, "Tech"}
  end
end

defmodule JobReaderTest do
  use ExUnit.Case, async: true
  doctest GeoHelper

  setup_all do
    project_dir = File.cwd!()
    profession_csv = Path.join([project_dir, "test", "data", "professions.csv"]) |> File.stream!()
    continents_json_path = Path.join([project_dir, "continents.json"])

    {:ok,
     profession_map: ProfessionReader.get_profession_mapping(profession_csv),
     geoms: GeoHelper.read_geometries(continents_json_path)}
  end

  test "Read jobs csv file", context do
    project_dir = File.cwd!()
    test_csv_path = Path.join([project_dir, "test", "data", "jobs.csv"])
    test_file = File.stream!(test_csv_path)

    result =
      JobReader.get_jobs(context[:profession_map], test_file, context[:geoms]) |> Enum.to_list()

    assert length(result) === 1
    assert List.first(result) === %{category_name: "Tech", continent: :europe}
  end

  test "Read jobs csv file with missing category id", context do
    project_dir = File.cwd!()
    test_csv_path = Path.join([project_dir, "test", "data", "jobs_with_missing_cat_id.csv"])
    test_file = File.stream!(test_csv_path)

    result =
      JobReader.get_jobs(context[:profession_map], test_file, context[:geoms]) |> Enum.to_list()

    assert length(result) === 1
    assert List.first(result) === %{category_name: "UNKNOWN", continent: :europe}
  end

  test "Read jobs csv file with invalid/missing coordinates", context do
    project_dir = File.cwd!()
    test_csv_path = Path.join([project_dir, "test", "data", "jobs_with_invalid_coordinates.csv"])
    test_file = File.stream!(test_csv_path)

    result =
      JobReader.get_jobs(context[:profession_map], test_file, context[:geoms]) |> Enum.to_list()

    assert length(result) === 2
    assert List.first(result) === %{category_name: "Tech", continent: :unlocalized}
    assert List.last(result) === %{category_name: "Tech", continent: :unlocalized}
  end
end
