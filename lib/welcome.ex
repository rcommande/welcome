defmodule Welcome do
  def run(profession_path, jobs_path, geom_path) do
    geoms = GeoHelper.read_geometries(geom_path)

    File.stream!(profession_path)
    |> ProfessionReader.get_profession_mapping()
    |> JobReader.get_jobs(File.stream!(jobs_path), geoms)
    |> Stats.compute()
    |> Renderer.render()
  end
end
