defmodule Stats do
  defp default_value(map, key, value, default) do
    case Map.has_key?(map, key) do
      true -> map[key] + value
      false -> default + value
    end
  end

  defp merge_stats(global_stats, stats) do
    updated_stats =
      stats
      |> Enum.map(fn {key, value} ->
        {key, default_value(global_stats, key, value, 0)}
      end)
      |> Map.new()

    Map.merge(global_stats, updated_stats)
  end

  defp compute_continent_stats(stats, job) do
    value = default_value(stats, job.category_name, 1, 0)
    new_stats = Map.put(stats, job.category_name, value)
    Map.put(new_stats, "Total", stats["Total"] + 1)
  end

  defp compute_job_category_stats(stats) do
    stats
    |> Enum.reduce(%{}, fn {_, category_stats}, acc ->
      merge_stats(acc, category_stats)
    end)
  end

  def compute(jobs) do
    stats =
      jobs
      |> Enum.reduce(
        %{
          :europe => %{"Total" => 0},
          :asia => %{"Total" => 0},
          :north_america => %{"Total" => 0},
          :south_america => %{"Total" => 0},
          :africa => %{"Total" => 0},
          :oceania => %{"Total" => 0},
          :unlocalized => %{"Total" => 0}
        },
        fn job, acc ->
          continent_stats = Map.get(acc, job.continent)
          new_continent_stats = compute_continent_stats(continent_stats, job)
          Map.put(acc, job.continent, new_continent_stats)
        end
      )

    Map.put(stats, :total, compute_job_category_stats(stats))
  end
end
