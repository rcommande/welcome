defmodule GeoHelper do
  def read_geometries(path) do
    continents =
      File.read!(path)
      |> Jason.decode!()
      |> Geo.JSON.decode!()

    continents.geometries
  end

  def continent_of_string(continent_name) do
    case continent_name do
      "Europe" ->
        :europe

      "Asia" ->
        :asia

      "North America" ->
        :north_america

      "South America" ->
        :south_america

      "Africa" ->
        :africa

      "Oceania" ->
        :oceania
    end
  end

  def get_continent(geoms, lat, lng) do
    point = %Geo.Point{coordinates: {lng, lat}}
    continent = Enum.find(geoms, fn geom -> Topo.contains?(geom, point) end)

    if continent do
      {:ok, continent_of_string(continent.properties["continent"])}
    else
      {:error, :lat_lng_error}
    end
  end
end
