NimbleCSV.define(CsvParser, separator: ",")

defmodule ProfessionReader do
  defp parse_csv([id, _, category_name]) do
    {String.to_integer(id), category_name}
  end

  def get_profession_mapping(file_stream) do
    file_stream
    |> CsvParser.parse_stream()
    |> Map.new(&parse_csv/1)
  end
end

defmodule JobReader do
  defp is_empty?(str) do
    str |> String.trim() === ""
  end

  defp clean_row(row) do
    [a, b, c, lat, lng] = row

    case {is_empty?(lat), is_empty?(lng)} do
      {true, _} -> [a, b, c, "0.0", "0.0"]
      {_, true} -> [a, b, c, "0.0", "0.0"]
      _ -> row
    end
  end

  defp make_job([id, _, _, lat, lng], geoms, profession_map) do
    continent_res = GeoHelper.get_continent(geoms, String.to_float(lat), String.to_float(lng))

    continent =
      case continent_res do
        {:ok, continent} -> continent
        {:error, :lat_lng_error} -> :unlocalized
      end

    profession_id =
      if is_empty?(id) do
        :unknown
      else
        String.to_integer(id)
      end

    %{
      category_name: Map.get(profession_map, profession_id, "UNKNOWN"),
      continent: continent
    }
  end

  def get_jobs(profession_map, file_stream, geoms) do
    file_stream
    |> CsvParser.parse_stream()
    |> Stream.map(&clean_row/1)
    |> Stream.map(fn row -> make_job(row, geoms, profession_map) end)
  end
end
