defmodule Renderer do
  defp get_headers(stats) do
    categories =
      Map.values(stats)
      |> Enum.map(fn stat -> Map.keys(stat) end)
      |> List.flatten()
      |> Enum.filter(fn item -> item != "Total" end)
      |> Enum.uniq()
      |> Enum.to_list()

    ["Total" | categories]
  end

  defp to_upper(enum) do
    Enum.map(enum, fn item -> String.upcase(item, :default) end)
  end

  defp format_values(stats, continent) do
    [String.upcase(Atom.to_string(continent), :default) | stats]
  end

  defp build_row(headers, continent, value) do
    Enum.map(headers, fn item -> Map.get(value, item, 0) end) |> format_values(continent)
  end

  def render(stats) do
    headers = get_headers(stats)

    total_row = build_row(headers, :total, stats[:total])

    rows = [
      total_row
      | Map.map(stats, fn {continent, value} -> build_row(headers, continent, value) end)
        |> Map.values()
        |> Enum.filter(fn item -> List.first(item) != "TOTAL" end)
    ]

    TableRex.quick_render!(rows, ["" | to_upper(headers)])
    |> IO.puts()
  end
end
